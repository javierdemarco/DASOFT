require './ChatbotDSL.rb'
# Utilizando el segundo enfoque se configura un bot de chat con una serie de saludos, despedidas,
# y entidades que tienen propiedades y comandos de creacion y eliminacion
c = ChatbotDSL.chatbot{
  greet_frase(:"Hola Humano")
  dismiss_frase(:"Adios Humano")
  unknown_frase(:"No entiendo lo que dices")
  end_command(:"Salir")
  entity(:"Event") do
    property(:"descipcion", :"String").required
    property(:"Aviso del evento", :"Reminder")
    property(:"Fecha del evento", :"Date").required
    property(:"id_entity", :"Int").required
    create_entity_commands(:"nuevo evento")
    create_entity_commands(:"añadir evento")
    create_entity_commands(:"+")
    delete_entity_commands(:"eliminar evento")
    delete_entity_commands(:"quitar evento")
    delete_entity_commands(:"-")
  end
  entity(:"Reminder") do
    property(:"Fecha del aviso", :"Date").required
  end
  entity(:"Date") do
    property(:"Dia", :"String").required
    property(:"Hora", :"String").required
  end
}
print("")