require './Entity.rb'
module ChatbotDSL 
  #Segundo enfoque de DSL, utilizando &proc para analizar los bloques de codigo
  def self.chatbot(&proc)
    @chatbot = Chatbot.new
    @chatbot.instance_eval(&proc)
    @chatbot
  end
  #Clase chatbot que representan al bot del chat
  class Chatbot
    #Inicializacion del chatbot
    def initialize()
      @greet_frase = nil
      @dismiss_frase = nil
      @unknown_frase = nil
      @end_command = nil
      @entities = Array.new
    end
    #Metodo de captacion de la palabra clave entity para creacion de entidades
    def entity(name, &proc)
      entity = Entity.new(name)
      entity.instance_eval(&proc)
      @entities.push(entity)
    end
    #Metodo para configuracion de la frase de saludo
    def greet_frase(frase)
      @greet_frase = frase
    end
    #Metodo para configuracion de la frase de despedida
    def dismiss_frase(frase)
      @dismiss_frase = frase
    end
    #Metodo para configuracion de la frase de cuando el bot no sabe que contestar
    def unknown_frase(frase)
      @unknown_frase = frase
    end
    #Metodo para configuracion del comando de acabar la conversacion
    def end_command(frase)
      @end_command = frase
    end
  end
end
