###
# Fichero de definicion del diagrama.
# Autor: Javier de Marco
###
require './Diagram.rb'

class DiagramDSL
  @@lastAdded
  @@conditionParent
  @@loopParent
  def self.diagram
    @@lastAdded = nil
    @diagram = Diagram.new
    yield if block_given?
  end
  # Metodo de agregacion de una actividad
  def self.activity(description)
    # Actividad inicial
    a = Activity.new(description)
    if @@lastAdded == nil
      a.initial()
      @diagram.add_child(a)
      @@lastAdded = a
    # Si el ultimo agregado es una actividad, lo agrego al padre de esa
    # actividad.
    elsif @@lastAdded.is_a?(Activity)
      parent = @@lastAdded.parent
      parent.add_child(a)
      @@lastAdded = a
    else
      # Si no es una actividad el ultimo agregado, lo agrego a el.
      parent = @@lastAdded
      parent.add_child(a)
      @@lastAdded = a
    end
    a
  end
  # Metodo de agregacion de una agrupacion
  def self.agrupation(description)
    # Error no se ha agregado una actividad inicial
    if @@lastAdded == nil
      raise RuntimeError,
          "La agrupacion #{description} no se puede agregar, ya que se necesita primera una actividad inicial", caller
    # Si el ultimo agregado es una actividad, lo agrego al padre de esa
    # actividad.
    elsif @@lastAdded.is_a?(Activity)
      parent = @@lastAdded.parent
      a = Agrupation.new(description)
      parent.add_child(a)
      @@lastAdded = a
    else
      # Si no es una actividad el ultimo agregado, lo agrego a el.
      parent = @@lastAdded
      a = Agrupation.new(description)
      parent.add_child(a)
      @@lastAdded = a
    end
    yield if block_given?
    @@lastAdded = a.parent
  end
  # Metodo de agregacion de un bucle
  def self.loop(description)
    # Error no se ha agregado una actividad inicial
    if @@lastAdded == nil
      raise RuntimeError,
          "El Bucle #{description} no se puede agregar, ya que se necesita primera una actividad inicial", caller
    # Si el ultimo agregado es una actividad, lo agrego al padre de esa
    # actividad.
    elsif @@lastAdded.is_a?(Activity)
      parent = @@lastAdded.parent
      a = Loop.new(description)
      parent.add_child(a)
      @@lastAdded = a
      @@loopParent = a
    else
      # Si no es una actividad el ultimo agregado, lo agrego a el.
      parent = @@lastAdded
      a = Loop.new(description)
      parent.add_child(a)
      @@lastAdded = a
      @@loopParent = a
    end
    yield if block_given?
    @@lastAdded = a.parent
  end
  # Metodo de agregacion de un camino de bucle
  def self.looppath(description)
    # Error no se ha agregado una actividad inicial
    if @@lastAdded == nil
      raise RuntimeError,
          "El Bucle #{description} no se puede agregar, ya que se necesita primera una actividad inicial", caller
    end
    # Si el ultimo agregado es una actividad, lo agrego al padre de esa
    # actividad.
    a = LoopPath.new(description)
    @@loopParent.add_child(a)
    @@lastAdded = a
    yield if block_given?
    @@lastAdded = a.parent
  end
  # Metodo de agregacion de una condicion
  def self.conditional(description)
    # Error no se ha agregado una actividad inicial
    if @@lastAdded == nil
      raise RuntimeError,
          "La Condicion #{description} no se puede agregar, ya que se necesita primera una actividad inicial", caller
    # Si el ultimo agregado es una actividad, lo agrego al padre de esa
    # actividad.
    elsif @@lastAdded.is_a?(Activity)
      parent = @@lastAdded.parent
      a = Condition.new(description)
      parent.add_child(a)
      @@lastAdded = a
      @@conditionParent = a
    else
      # Si no es una actividad el ultimo agregado, lo agrego a el.
      parent = @@lastAdded
      a = Condition.new(description)
      parent.add_child(a)
      @@lastAdded = a
      @@conditionParent = a
    end
    yield if block_given?
    @@lastAdded = a.parent
  end
  # Metodo de agregacion de un camino de una condicion
  def self.conditionalpath(description)
    # Error no se ha agregado una actividad inicial
    if @@lastAdded == nil
      raise RuntimeError,
          "El camino de la Condicion #{description} no se puede agregar, ya que se necesita primera una actividad inicial", caller
    # Si el ultimo agregado es una actividad, lo agrego al padre de esa
    # actividad.
    end
    a = ConditionPath.new(description)
    @@conditionParent.add_child(a)
    @@lastAdded = a
    yield if block_given?
    @@lastAdded = a.parent
  end
  # Metodo de obtencion del diagrama
  def self.get_diagrama
    @diagram
  end
end