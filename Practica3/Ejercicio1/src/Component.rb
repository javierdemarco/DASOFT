###
# Fichero de definicion de los componentes del diagrama.
# Autor: Javier de Marco
###
# Clase componente. Sera el punto de partida para el patron composite.
# Se definira con una descripcion y una serie de hijos.
# El atributo parent se define segun se agregan hijos.
class Component
  # Constructor del componente
  def initialize(description)
    @description = description
    @children = Array.new
    @exe_code = nil
  end
  #Metodo de agregacinode un hijo del patron
  def add_child(component)
    # Si es una actividad inicial, no podra haber ninguna antes, 
    # si es final no podra haber ninguna despues
    if component.is_a?(Activity)
      if component.is_initial() == true 
        raise RuntimeError,
        "La actividad #{component.description} es inicial y no es posible añadirla.", caller
      elsif component.is_final() == true
        if @children[-1] != nill && @children[-1].is_final() == true
          raise RuntimeError,
          "La actividad #{component.description} es final y la anterior tambien.", caller
        end
      elsif component.is_final() == false
        @children.push(component)
        component.parent = self
      end
    # Cualquier otro componente simplemente se agrega
    elsif component.is_a?(Condition) || component.is_a?(Loop) || component.is_a?(Agrupation) ||
      component.is_a?(ConditionPath) || component.is_a?(LoopPath)
      @children.push(component)
      component.parent = self
    else
      raise RuntimeError,
          "No se pudo agregar el componente: #{component.description}", caller
    end
  end
  # Metodo de obtencion de un hijo por indice
  def get_child(index)
    @children[index]
  end
  # Metodo de obtencion del array de hijos
  def get_children()
    @children
  end
  #Metodo de obtencion de cuantos caminos tiene el condicional
  def get_children_size
    @children.size()
  end
  attr_reader :description, :children
  attr_accessor :parent
end
# Clase actividad. Es un componentes el cual no tendra hijos.
# Es un elemento simple del diagrama que representa una accion.
# Podra ser inicial o final.
class Activity < Component
  # Constructor de la actividad
  def initialize(description)
    super(description)
    @children = nil
    @initial = false
    @final = false
  end
  # Metodo de definicion para una actividad initial
  def initial; @initial = true end
  # Metodo de definicion para una actividad final.
  def final; @final = true end
  def is_initial()
    @initial
  end
  def is_final()
    @final
  end
end
# Clase Agrupacion. Se trata de un componente que tendra definido un nombre,
# Y una serie de componentes contenidos. Se utiliza para definir por nombre una
# parte del diagrama.
class Agrupation < Component
  # Constructor de la agrupacion
  def initialize(name)
    super(name)
  end
end
# Clase Bucle. Componente del diagrama que define un bucle de ejecucion.
# Tiene una condicion y esta definida por dos componentes, para sus dos
# caminos posibles.
class Loop < Component
  # Constructor del bucle
  def initialize(condition)
    super(condition)
  end
  attr_accessor :condition
end
# Clase Camino Bucle. Define un camino del bucle en el diagrama.
# Se trata de un componente simple.
class LoopPath < Component
  # Constructor del camino del bucle
  def initialize(name)
    super(name)
  end
end
# Clase Condicion. Define condiciones y sus caminos posibles.
# Este no podra ser un unico camino, debera ser 2 o mas.
# Esos caminos estaran definidos en children de Condition.
class Condition < Component
  # Constructor de la condicion
  def initialize(condition)
    super(condition)
  end
end
# Clase Camino de Condicion. Define un camino para una condicion.
# Tendra un nombre textual y una serie de hijos que sera los componentes del camino.
class ConditionPath < Component
  # Constructor del camino de la condicion
  def initialize(condition)
    super(condition)
  end
end
