require './DiagramDSL.rb'

class DiagramaActividad < DiagramDSL
  diagram{
    activity(:"leer numero de entrada").initial
    conditional(:"numero es cero?") do
      conditionalpath(:"si") do
        activity(:"terminar").final
      end
      conditionalpath(:"no") do
        agrupation(:"inicializacion") do
          activity(:"crear lista de divisores vacia")
          activity(:"crear contador = 1")
        end
        agrupation(:"algoritmo") do
          loop(:"contador es distinto de numero?") do
            looppath(:"si") do
              activity(:"dividir numero entre contador")
              conditional(:"resto es cero?") do
                conditionalpath(:"si") do
                  activity(:"guardar numero en lista de divisores")
                end
                conditionalpath(:"no") do
                end
              end
              activity(:"sumar 1 a contador")
            end
            looppath(:"no") do
              activity(:"mostrar resultado").final
            end
          end
        end
      end
    end
  }

end

a = DiagramaActividad.get_diagrama()
a.check_diagram()
a.generate_uml()
system("java -jar plantuml.jar ./Diagram.uml")