require './Citizen'
require './Colective'
require './Proyect'
#Definicion de las excepciones personalizadas
class ProyectFollowException < RuntimeError
  def initialize (id)
    @id = id
  end
  attr_reader :id
end
#Clase App, que contendra la "Base de datos" y las operaciones de las demas clases
class App
  def initialize()
    @members = Array.new
    @proyects = Array.new
    @c_login = nil
    @n_proyects = 1
  end
  # Metodo de registro en la app
  def registry()
    #Comienzo del Registro, pedir por pantalla el nombre de usuario
    puts "Vamos a realizar el registro de un usuario"
    puts "Por favor introduce un nombre de usuario"
    begin 
      username = gets.chomp
      if @members.find{|c| c.is_member(username)}
        puts "El nombre de usuario introducido ya existe, pruebe otra vez"
      end
    end while @members.find{|c| c.is_member(username)} != nil
    #Pido el NIF y lo guardo en una variable
    puts "Por favor introduce tu NIF"
    begin 
      nif = gets.chomp
      if @members.find{|c| c.is_member(nif)}
        puts "El nif introducido ya existe, pruebe otra vez"
      end
    end while @members.find{|c| c.is_member(nif)} != nil
    #Pido la contraseña y la guardo en una variable
    puts "Por favor introduce tu contraseña"
    password = gets.chomp
    #Creo el ciudadano y lo introduzco en la base de datos
    @members.push(Citizen.new(username, nif, password))
  end
  # Metodo de log in en la app
  def login()
    citizens = @members.select{|c| c.is_a?(Citizen)}
    puts "Introduce el nombre de usuario o nif"
    username = gets.chomp
    puts "Introduce la contraseña"
    password = gets.chomp
    @c_login = citizens.find{|c| c.is_member(username) && c.password == password}
    if @c_login != nil
      puts "Has iniciado sesion con #{@c_login.username}"
      puts "Login completado\n\n"
    else
      puts "No existe esa combinacion de usuario y contraseña"
    end
  end
  # Metodo de logout
  def logout()
    @c_login = nil
  end
  # Metodo de creacion de un colectivo, el usuario logeado sera el representante
  def colective_create()
    puts "Introduce el nombre del colectivo"
    begin 
      name = gets.chomp
      # Si encuentro el nombre en la lista indico el error y pido uno nuevo
      if @members.find{|c| c.is_member(name)}
        puts "Ese nombre de colectivo ya existe, prueba con otro"
      end
    end while @members.find{|c| c.is_member(name)} != nil
    # Como es posible que introduzca mal la opcion si/no
    # Esta en un bucle switch para correctamente guardar el interes del colectivo
    begin
      puts "¿Desea introducir un interes del colectivo? si/no"
      option = gets.chomp
      case option
        when "si"
          puts "Introduzca el interes"
          interest = gets.chomp
        when "no"
          interest = "-"
        else
          puts "Esa opcion no es correcta\n\n"
      end
    end while interest == nil
    # Una vez captada la info se crea el colectivo, con ciudadanos y subcolectivo vacios
    @members.push(Colective.new(name, @c_login, nil, interest))
    puts "Colectivo creado correctamente\n\n"
  end
  # Metodo de registro en un colectivo
  def colective_registry()
    # Unicamente podra darse de alta en aquellos colectivos, que no pertenezca.
    # Eso incluye a los sub-colectivos de aquellos a los que ya se pertenezca
    # Asi como los cuales ya eres un representane.
    # Basicamente aquellos en los que no se pertenezca directa o indirectamente
    col_options = @members.select{|c| !c.is_member(@c_login) && c.is_a?(Colective)}
    if !col_options.empty?
      puts "Estos son los colectivos a los que se puede unir"
      col_options.each_with_index{|c, index| puts "#{index}: #{c}\n\n"}
      puts "Escribe el nombre del colectivo al que quieres unirte"
      puts "Si no quieres unirte ninguno introduce no"
      begin 
        option_name = gets.chomp
        if option_name == "no"
          break
        end
        # si no encuentro el nombre en la lista de opciones disponibles doy error y pido otro
        col_reg = col_options.find{|c| c.is_member(option_name)}
        if col_reg == nil
          puts "Ese nombre de colectivo no existe, prueba con otro"
        end
      end while col_reg == nil
      # Agrego el miembro
      if col_reg != nil
        col_reg.add_member(@c_login)
        puts "Se ha registrado dentro del colectivo correctamente"
      end
      
    end
  end
  # Metodo para que un usuario se de de baja de un colectivo
  def colective_drop()
    # Unicamente podra darse de baja en aquellos colectivos, que pertenezca.
    # Eso incluye a los sub-colectivos de aquellos a los que ya se pertenezca
    # Asi como los cuales ya eres un representante.
    # Basicamente aquellos en los que se pertenezca directa o indirectamente
    col_options = @members.select{|c| c.is_member(@c_login) && c.is_a?(Colective) && !c.is_rep(@c_login)}
    if !col_options.empty?
      puts "Estos son los colectivos de los que puede darse de baja"
      col_options.each_with_index{|c, index| puts "#{index}: #{c}\n\n"}
      puts "Escribe el nombre del colectivo que quieres abandonar"
      puts "Si no quieres abandonar ninguno introduce no"
      begin 
        option_name = gets.chomp
        if option_name == "no"
          break
        end
        # Si no encuentro el nombre doy error y pido el nuevo
        col_reg = col_options.find{|c| c.is_member(option_name)}
        if col_reg == nil
          puts "Ese nombre de colectivo no existe, prueba con otro"
        end
      end while col_reg == nil
      # Elimino el miembro
      if col_reg != nil
        col_reg.delete_member(@c_login)
        puts "Se ha eliminado del colectivo correctamente"
      end
    end
  end
  # Metodo para que un representante de de alta su colectivo en otro colectivo
  def colective_registry_rep()
    # Unicamente podra darse de alta en aquellos colectivos, que no pertenezca.
    # Eso incluye a los sub-colectivos de aquellos a los que ya se pertenezca
    # Asi como los cuales ya es un representane.
    # Basicamente aquellos en los que no se pertenezca directa o indirectamente
    colectives = @members.select{|c| c.is_a?(Colective)}
    if !colectives.select{|c| c.is_rep(@c_login)}.empty?
      puts "¿Con cual de los colectivos a los que representas quieres operar?"
      colectives.select{|c| c.is_rep(@c_login)}.each{|d| print d}
      begin 
        name = gets.chomp
        # Si el colectivo no es correcto pido otro nombre
        col_rep = colectives.find{|c| c.is_member(name) && c.is_rep(@c_login)}
        if col_rep == nil
          puts "El colectivo seleccionado no es correcto, prueba otra vez"
        end
      end while col_rep == nil
      # Las opciones son aquellos colectivos que no es miembro y el nombre no es el mismo
      # Asi se evita que se pueda unir un colectivo a si mismo
      col_options = @members.select{|c| !c.is_member(col_rep) && c.is_a?(Colective) && !c.is_member(col_rep.name)}
      if !col_options.empty?
        puts "Estos son los colectivos a los que se puede unir"
        col_options.each_with_index{|c, index| puts "#{index}: #{c}\n\n"}
        puts "Escribe el nombre del colectivo al que quieres unirte"
        puts "Si no quieres unirte ninguno introduce no"
        begin 
          option_name = gets.chomp
          if option_name == "no"
            break
          end
          # Si no encuentro el nombre doy error y pido otro
          col_reg = col_options.find{|c| c.is_member(option_name)}
          if col_reg == nil
            puts "Ese nombre de colectivo no existe, prueba con otro"
          end
        end while col_reg == nil
        # Agrego el miembro
        if col_reg != nil
          col_reg.add_member(col_rep)
          puts "Se ha registrado dentro del colectivo correctamente"
        end
      end
    end
  end
  # Metodo para que un representante de de baja su colectivo en otro colectivo
  def colective_drop_rep()
    colectives = @members.select{|c| c.is_a?(Colective)}
    puts "¿Con cual de los colectivos a los que representas quieres operar?"
    if !colectives.select{|c| c.is_rep(@c_login)}.empty?
      # Aquellos colectivos que soy representante
      colectives.select{|c| c.is_rep(@c_login)}.each{|d| print d}
      begin 
        name = gets.chomp
        # Si el nombre no es correcto doy error y pido otro
        col_rep = colectives.find{|c| c.is_member(name) && c.is_rep(@c_login)}
        if col_rep == nil
          puts "El colectivo seleccionado no es correcto, prueba otra vez"
        end
      end while col_rep == nil
      # Aquellos colectivos que soy miemmbro y el nombre no es igual.
      # Asi se evita salir de si mismo en el colectivo
      col_options = @members.select{|c| c.is_member(col_rep) && c.is_a?(Colective) && !c.is_member(col_rep.name)}
      if !col_options.empty?
        puts "Estos son los colectivos que puedes abandonar"
        col_options.each_with_index{|c, index| puts "#{index}: #{c}\n\n"}
        puts "Escribe el nombre del colectivo que quieres abandonar"
        puts "Si no quieres unirte ninguno introduce no"
        begin 
          option_name = gets.chomp
          if option_name == "no"
            break
          end
          col_reg = col_options.find{|c| c.is_member(option_name)}
          if col_reg == nil
            puts "Ese nombre de colectivo no existe, prueba con otro"
          end
        end while col_reg == nil
        # Elimino el miembro
        if col_reg != nil
          col_reg.delete_member(col_rep)
          puts "Se ha registrado dentro del colectivo correctamente"
        end
      end
    end
  end
  # Merodo de creacion de proyectos como ciudadano
  def proyect_create()
    puts "Introduce el titulo del proyecto"
    title = gets.chomp
    # Bucle para introducir una descripcion o no
    begin
      puts "¿Desea introducir una descripcion del proyecto? si/no"
      option = gets.chomp
      case option
        when "si"
          puts "Introduzca la descipcion"
          desc = gets.chomp
        when "no"
          desc = "-"
        else
          puts "Esa opcion no es correcta\n\n"
      end
    end while desc == nil
    @proyects.push(Proyect.new(title, desc, @c_login, @n_proyects))
    @n_proyects += 1
  end
  # Merodo de eliminar de proyectos como ciudadano
  def proyect_delete()
    puts "Estos son los proyectos que tienes propuestos"
    options = @proyects.select{|c| c.proponent == @c_login}
    if !options.empty?
      options.each{|c| print c}
      puts "Cual es el proyecto que quieres eliminar, introduce el id"
      puts "Si no quieres eliminar introduce 0"
      begin
        op = gets.chomp.to_i
        if op == 0
          break
        end
        if options.find{|c| c.id == op}
          @proyects.delete(options.find{|c| c.id == op})
          @n_proyects -= 1
        else
          puts "Esa opcion no es correcta, prueba otra vez"
        end
      end while op == nil
    end
  end
  # Merodo de creacion de proyectos como Colectivo
  def proyect_create_rep()
    colectives = @members.select{|c| c.is_a?(Colective)}
    if !colectives.select{|c| c.is_rep(@c_login)}.empty?
      puts "¿Con cual de los colectivos a los que representas quieres operar?"
      colectives.select{|c| c.is_rep(@c_login)}.each{|d| print d}
      begin 
        name = gets.chomp
        # Si el colectivo no es correcto pido otro nombre
        col_rep = colectives.find{|c| c.is_member(name) && c.is_rep(@c_login)}
        if col_rep == nil
          puts "El colectivo seleccionado no es correcto, prueba otra vez"
        end
      end while col_rep == nil
      puts "Introduce el titulo del proyecto"
      title = gets.chomp
      # Bucle para introducir una descripcion o no
      begin
        puts "¿Desea introducir una descripcion del proyecto? si/no"
        option = gets.chomp
        case option
          when "si"
            puts "Introduzca la descipcion"
            desc = gets.chomp
          when "no"
            desc = "-"
          else
            puts "Esa opcion no es correcta\n\n"
        end
      end while desc == nil
      @proyects.push(Proyect.new(title, desc, col_rep, @n_proyects))
      @n_proyects += 1
    end
  end
  # Merodo de eliminar de proyectos como colectivo
  def proyect_delete_rep()
    colectives = @members.select{|c| c.is_a?(Colective)}
    if !colectives.select{|c| c.is_rep(@c_login)}.empty?
      puts "¿Con cual de los colectivos a los que representas quieres operar?"
      colectives.select{|c| c.is_rep(@c_login)}.each{|d| print d}
      begin 
        name = gets.chomp
        # Si el colectivo no es correcto pido otro nombre
        col_rep = colectives.find{|c| c.is_member(name) && c.is_rep(@c_login)}
        if col_rep == nil
          puts "El colectivo seleccionado no es correcto, prueba otra vez"
        end
      end while col_rep == nil
      puts "Introduce el titulo del proyecto"
      title = gets.chomp
      # Bucle para introducir una descripcion o no
      begin
        puts "¿Desea introducir una descripcion del proyecto? si/no"
        option = gets.chomp
        case option
          when "si"
            puts "Introduzca la descipcion"
            desc = gets.chomp
          when "no"
            desc = "-"
          else
            puts "Esa opcion no es correcta\n\n"
        end
      end while desc == nil
      @proyects.push(Proyect.new(title, desc, col_rep, @n_proyects))
      @n_proyects -= 1
    end
  end
  # Metodo de Apoyo a proyectos de un Ciudadano
  def proyect_follow()
    puts "Estos son los proyectos"
    @proyects.each{|c| print c}
    puts "indica el id del proyecto que quieres apoyar"
    begin
      id = gets.chomp.to_i
      proyect = @proyects.find{|c| c.id == id}
      if proyect == nil
        puts "Ese id no es correcto intenta con otro"
      end
    end while proyect == nil
    #Excepcion 1, Que el usuario ya pertenezca a los que apoyan el proyecto o que sea el proponente
    if proyect.followers.find{|c| c.is_member(@c_login)} || proyect.proponent == @c_login
      raise ProyectFollowException.new(id), "El usuario ya esta apoyando el proyecto", caller
    #Excepcion 2, que la fecha del proyecto sea mayor de 60 dias
    elsif (DateTime.now - proyect.creation_date).to_i > 60
      raise ProyectFollowException.new(id), "El proyecto es demasiado antiguo", caller
    else # Si no ha ocurrido ningun error continuo con la ejecucion
      proyect.followers.push(@c_login)
    end
  end
  # Metodo de apoyo a proyectos como Colectivo
  def proyect_follow_rep()
    colectives = @members.select{|c| c.is_a?(Colective)}
    if !colectives.select{|c| c.is_rep(@c_login)}.empty?
      puts "¿Con cual de los colectivos a los que representas quieres operar?"
      colectives.select{|c| c.is_rep(@c_login)}.each{|d| print d}
      begin 
        name = gets.chomp
        # Si el colectivo no es correcto pido otro nombre
        col_rep = colectives.find{|c| c.is_member(name) && c.is_rep(@c_login)}
        if col_rep == nil
          puts "El colectivo seleccionado no es correcto, prueba otra vez"
        end
      end while col_rep == nil
      puts "Estos son los proyectos"
      @proyects.each{|c| print c}
      puts "indica el id del proyecto que quieres apoyar"
      begin
        id = gets.chomp.to_i
        proyect = @proyects.find{|c| c.id == id}
        if proyect == nil
          puts "Ese id no es correcto intenta con otro"
        end
      end while proyect == nil
      #Excepcion 1, Que el usuario ya pertenezca a los que apoyan el proyecto o que sea el proponente
      if proyect.followers.find{|c| c.is_member(col_rep)} || proyect.proponent == col_rep
        raise ProyectFollowException.new(id), "El Colectivo ya esta apoyando el proyecto", caller
      #Excepcion 2, que la fecha del proyecto sea mayor de 60 dias
      elsif (DateTime.now - proyect.creation_date).to_i > 60
        raise ProyectFollowException.new(id), "El proyecto es demasiado antiguo", caller
      else # Si no ha ocurrido ningun error continuo con la ejecucion
        if col_rep.members != nil
          col_rep.members.each{|c| proyect.followers.delete(c)}
        end
        proyect.followers.delete(col_rep.c_rep)
        proyect.followers.push(col_rep)
      end
    end
    
  end
  # Metodo de creacion de informes de popularidad
  def report_popularity()
    File.open("../reports/report_popularity.txt", "w") do |file|
      @proyects.each{|c| 
        file.print "Proyecto: #{c.id}\nPopularidad: #{c.popularity()}\n#{c}\n\n"
        }
    end
    puts "Informe de popularidad completado"
  end
  # Metodo de creacion de informes de afinidad entre colectivos
  def report_affinity()
    puts "Comenzando informe\n"
    File.open("../reports/report_affinity.txt", "w") do |file|
      colectives = @members.select{|c| c.is_a?(Colective)}
      colectives.each{|c| colectives.each{|d| 
        if c != d then
          aff = calculate_affinity(c, d)
          file.puts "Afinidad entre #{c.name} y #{d.name}: #{aff.to_f}"
        end
      }}
      puts "Informe de afinidad completado\n"
    end
  end
  # Metodo para calcular la afinidad entre dos colectivos
  def calculate_affinity(c1, c2)
    proyects1 = @proyects.select{|c| c1 == c.proponent}
    proyects2 = @proyects.select{|c| c2 == c.proponent}
    n_p2_in_p1 = proyects1.select{|c| c.followers.include?(c2)}.count
    n_p1_in_p2 = proyects2.select{|c| c.followers.include?(c1)}.count
    affinity = (n_p1_in_p2.to_f + n_p2_in_p1.to_f) / @n_proyects.to_f
    affinity
  end
  attr_reader :members, :c_login, :proyects, :n_proyects
end
