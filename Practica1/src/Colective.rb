class Colective
  def initialize(name, c_rep ,members, interest)
    @name = name # nombre del colectivo
    @c_rep = c_rep #ciudadano que lo representa
    @members = members # Miembros del colectivo, ya sean ciudadnos o sub-colectivos
    @interest = interest # interes comun
  end
  # Metodo que comprueba si un objeto es miembro de un colectivo, ya sea como representante
  # O como miembro de algun del grupo miembros
  def is_member(test_c)
    if test_c.is_a?(Citizen)
      if @c_rep == test_c
        true
      elsif @members != nil && @members.find{|c| c.is_member(test_c)} != nil
        true
      else
        false
      end
    elsif test_c.is_a?(Colective)
      if @name == test_c.name
        true
      elsif @members != nil && @members.find{|c| c.is_member(test_c)} != nil
        true
      else
        false
      end
    elsif test_c.is_a?(String)
      @name == test_c
    else
      false
    end
  end
  # Metodo booleano que indica si un objeto es el representante de un colectivo
  def is_rep(test_c)
    if !test_c.is_a?(String)
      @c_rep == test_c
    else
      @name == test_c.username
    end
  end
  # Metodo que añade un objeto al grupo de miembros
  def add_member(c)
    if @membes == nil
      @members = Array.new
    end
    @members.push(c)
  end
  # Metodo que elimina un objeto del grupo de miembros
  def delete_member(c)
    @members.delete(c)
  end
  # Metodo que calcula el numero de miembros del colectivo
  def n_members()
    cont = 1
    if @members != nil
      cont += @members.count()
      @members.each{|c| if c.is_a?(Colective) then cont += c.n_members() end}
    end
    cont
  end
  # Metodo to string
  def to_s()
    "Name: #{@name}\nc_rep: #{@c_rep}\nInterest: #{@interest}\nMembers: #{@members}\n\n"
  end

  attr_reader :name, :c_rep, :interest, :members
end
