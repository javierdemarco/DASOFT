require 'date'
# Clase que identifica un proyecto en el sistema
# Tendra un titulo, descripcion, proponente, id unico, y una fecha de creacion.
class Proyect
  def initialize(title, description, proponent, id)
    @title = title
    @description = description
    @proponent = proponent
    @id = id
    @creation_date = DateTime.now
    @followers = Array.new
  end
  # Metodo de calculo de popularidad de un proyecto
  def popularity()
    count = 1 # Uno por el proponente
    if !@followers.empty?
      @followers.each{|c|
        if c.is_a?(Citizen) then count += 1 else count += c.n_members() end
      }
    else
      count
    end
    count
  end
  # Metodo to string
  def to_s()
    "title: #{@title}
    description: #{@description}
    id: #{@id}
    Date: #{@creation_date}
    proponent: #{@proponent}
    followers: #{@followers}\n\n"
  end

  attr_reader :title, :description, :proponent, :id, :creation_date, :followers
end