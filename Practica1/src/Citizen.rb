class Citizen
  def initialize(username, nif, password)
    @username = username
    @nif = nif
    @password = password
  end

  def is_member(c)
    if c.is_a?(Citizen)
      @username == c.username
    elsif c.is_a?(String)
      @username == c || @nif == c
    else
      false
    end
  end

  def change_password()
    puts "Introduce tu contraseña actual"
    begin
      new_pass = gets.chomp
      if new_pass != @password
        puts "Esa contraseña no es tu actual contraseña" 
        new_pass = nil
      end
    end while new_pass == nil
    puts "Introduce la contraseña nueva"
    new_pass = gets.chomp
    @password = new_pass
  end

  def to_s()
    " Username: #{@username}, NIF: #{@nif}"
  end

  attr_reader :username, :nif, :password
end
  