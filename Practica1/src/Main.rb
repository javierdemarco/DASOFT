require './App.rb'
#---------------------------EMPIEZA LA APP---------------------------------------#
#Inizializo la 'Base de datos' de ciudadanos
app = App.new
# Flag para navegacion de los menus 0 = principal tras login
# 1 = colectivos, 2 = proyectos, 3 = Informes
flag_menu = 0
while 1
  # Menu principal, sin haber hecho login
  while app.c_login == nil
    #Menu Inicio
    puts "Hola, por favor escoja la opcion que desee"
    puts "Opcion 1: Registrar usuario"
    puts "Opcion 2: Ver los usuarios registrados"
    puts "Opcion 3: Ver los Colectivos registrados"
    puts "Opcion 4: Entrar en la aplicacion"
    puts "Opcion 5: Salir\n\n"
    STDOUT.flush
    option = gets.chomp.to_i
    case option
      when 1 # Registro
        puts "Opcion 1 introducida"
        #Opcion Registrar usuario
        app.registry()
        puts "Registro completado\n\n"
      when 2 # Visualizacion de usuarios registrados
        puts "Opcion 2 introducida\n"
        puts "Usuarios registrados:"
        app.members.each_with_index{|c, index| if c.is_a?(Citizen) then puts "Usuario #{index}:\n #{c}" end}
        puts "\n"
      when 3 # Visualizacion de usuarios registrados
        puts "Opcion 3 introducida\n"
        puts "Colectivos registrados:"
        app.members.each_with_index{|c, index| if c.is_a?(Colective) then puts "Colectivo #{index}:\n #{c}" end}
        puts "\n"
      when 4 # Inicio de sesion
        puts "Opcion 4 introducida, Log In\n"
        app.login()
        @flag_menu = 0
      when 5 #Salir
        puts "Opcion 5 introducida, saliendo de la App\n\n"
        exit
      else
        puts "Esa opcion no es correcta\n\n"
    end
  end
  # Menu despues de hacer login
  while @flag_menu == 0 && app.c_login != nil
    # Menu con un usuario logeado
    puts "Opcion 1: Cambiar contraseña"
    puts "Opcion 2: Operaciones con colectivos"
    puts "Opcion 3: Operaciones con proyectos"
    puts "Opcion 4: Operaciones con informes"
    puts "Opcion 5: Logout\n\n"
    option = gets.chomp.to_i
    case option
      when 1 # Crear colectivo
        puts "Opcion 1 escogida, iniciando proceso de cambio de contraseña"
        user = app.members.find{|c| c.is_member(app.c_login) && c.is_a?(Citizen)}
        user.change_password()
      when 2 # Operaciones con Colectivos
        puts "Opcion 2 introducida, Operaciones con colectivos\n\n"
        @flag_menu = 1
        break
      when 3 # Operaciones con Proyectos
        puts "Opcion 3 introducida, Operaciones con proyectos\n\n"
        @flag_menu = 2
        break
      when 4 # Operaciones con informes
        puts "Opcion 4 introducida, operaciones con informes"
        @flag_menu = 3
        break
      when 5 # Logout, vuelve al menu principal
        puts "Opcion 5 introducida, logout, volviendo al menu principal\n\n"
        app.logout()
        @flag_menu = 0
      else
        puts "Esa opcion no es correcta\n\n"
    end
  end
  # Menu para colectivos
  while @flag_menu == 1 && app.c_login != nil
    puts "Opcion 1: Crear colectivo"
    puts "Opcion 2: Visualizar colectivos a los que se pertenece"
    puts "Opcion 3: Visualizar colectivos que represento"
    puts "Opcion 4: Darse de alta en un colectivo como ciudadano"
    puts "Opcion 5: Darse de baja en un colectivo como ciudadano"
    puts "Opcion 6: Darse de alta en un colectivo, con el colectivo al que representa"
    puts "Opcion 7: Darse de baja en un colectivo, con el colectivo al que representa"
    puts "Opcion 8: Atras"
    option = gets.chomp.to_i
    case option
      when 1 # Crear colectivo
        puts "Opcion 1 escogida, creando un colectivo"
        app.colective_create()
      when 2 # Visualizar los colectivos a los que se pertenece
        puts "Opcion 2 escogida, Estos son los colectivos a los que perteneces como ciudadano"
        colectives = app.members.select{|c| c.is_a?(Colective)}
        colectives.select{|c| c.is_member(app.c_login) && !c.is_rep(app.c_login)}.each{|d| print d}
      when 3 # Visualizar los colectivos a los que se representa
        puts "Opcion 3 escogida, Estos son los colectivos que representas"
        colectives = app.members.select{|c| c.is_a?(Colective)}
        colectives.select{|c| c.is_rep(app.c_login)}.each{|d| print d}
      when 4 # Darse de alta en un colectivo
        puts "Opcion 4 escogida, procediendo a darse de alta en un colectivo como ciudadano"
        app.colective_registry()
      when 5 # Darse de baja en un colectivo
        puts "Opcion 5 escogida, procediendo a darse de baja en un colectivo, como ciudadano"
        app.colective_drop()
      when 6 # Darse de alta en un colectivo, como representante.
        puts "Opcion 6 escogida, procediendo a darse de alta en un colectivo como representante"
        app.colective_registry_rep()
      when 7 # Darse de baja en un colectivo
        puts "Opcion 7 escogida, procediendo a darse de baja en un colectivo como representante"
        app.colective_drop_rep()
      when 8 # Atras
        puts "Opcion 8 escogida, atras"
        @flag_menu = 0
        break
      else
        puts "Opcion no disponible"
    end
  end
  # Menu para Proyectos
  while @flag_menu == 2 && app.c_login != nil
    # Menu para operaciones sobre proyectos
    puts "Opcion 1: Crear Proyecto como Ciudadano"
    puts "Opcion 2: Eliminar Proyecto como Ciudadano"
    puts "Opcion 3: Crear Proyecto como Colectivo"
    puts "Opcion 4: Eliminar Proyecto como Colectivo"
    puts "Opcion 5: Apoyar proyecto como Ciudadano"
    puts "Opcion 6: Apoyar proyecto como Colectivo"
    puts "Opcion 7: Ver Proyectos a los que pertenezco como Ciudadano"
    puts "Opcion 8: Atras\n\n"
    option = gets.chomp.to_i
    case option
      when 1 # Crear Proyecto como Ciudadano
        puts "Opcion 1 escogida, iniciando proceso creacion de proyecto como ciudadano"
        app.proyect_create()
      when 2 # Eliminar Proyecto como Ciudadano
        puts "Opcion 2 escogida, iniciando proceso eliminacion de proyecto como ciudadano"
        app.proyect_delete()
      when 3 # Crear Proyecto como Representante de un colectivo
        puts "Opcion 3 escogida, iniciando proceso creacion de proyecto como Colectivo"
        app.proyect_create_rep()
      when 4 # Eliminar Proyecto como Colectivo
        puts "Opcion 4 escogida, iniciando proceso eliminacion de proyecto como Colectivo"
        app.proyect_delete_rep()
      when 5 # Apoyar proyecto como ciudadano
        puts "Opcion 5 escogida, iniciando proceso apoyar proyecto como Ciudadano"
        begin
          app.proyect_follow()
        rescue ProyectFollowException => error
          print error, " ", error.id, "\n\n"
        else
          puts "Apoyo al proyecto realizado correctamente"
        end
      when 6 # Apoyar proyecto como Colecgivo
        puts "Opcion 6 escogida, iniciando proceso apoyar proyecto como Colectivo"
        begin
          app.proyect_follow_rep()
        rescue ProyectFollowException => error
          print error, " ", error.id, "\n\n"
        else
          puts "Apoyo al proyecto realizado correctamente"
        end
      when 7 # Ver proyectos que participo como ciudadano
        puts "Opcion 7 escogida, Estos son los proyectos a los que perteneces"
        app.proyects.select{|c| c.proponent == app.c_login}.each{|c| print c}
        proyect = app.proyects.select{|c| !c.followers.empty?}
        proyect.select{|c| c.followers.find{|c| c.is_member(app.c_login)}}.each{|c| print c}
      when 8 # Atras
        puts "Opcion 8 introducida, Atras\n\n"
        @flag_menu = 0
      else
        puts "Esa opcion no es correcta\n\n"
    end
  end
  # Menu para informes
  while @flag_menu == 3 && app.c_login != nil
    puts "Opcion 1: Crear Informe de popularidad"
    puts "Opcion 2: Crear Informe de afinidad entre colectivos"
    puts "Opcion 3: Atras"
    option = gets.chomp.to_i
    case option
      when 1 # Opcion 1 Crear informe de popularidad
        app.report_popularity()
      when 2 # Opcion 2 Crear informe de afinidad entre colectivos
        app.report_affinity()
      when 3 # Atras
        puts "Opcion 3 introducida, atras\n\n"
        @flag_menu = 0
      else
        puts "Esa opcion no es correcta\n\n"
    end
  end
end
